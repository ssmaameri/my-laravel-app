<?php

namespace App\Services;

class GitUpdateService
{

    private static $bitbucketIps = [
        '104.192.136.0/21',
        '34.198.203.127',
        '34.198.178.64',
        '34.198.32.85',
        '127.0.0.1'
        ];

    private static $apiKey = 'BVL1aqsupH5aM3zHioUOv2oA5FJN5r6k';

    /**
     * Update the applications git repository
     * @param string the IP where the request came from: $_SERVER['REMOTE_ADDR']
     * @param array the URI parameters: $_GET
     * @return array response
     */
    public static function update($remoteAddress, $parameters){

        if(!self::validRequest($remoteAddress, $parameters)){
            return json_encode([
                'status' => 404
            ]);
        }

        # bring remote references to date
        exec('git remote update 2>&1', $gitUpdate);

        # get status of current branch
        exec('git status -sb 2>&1', $gitStatus);

        $pullResponse = null;
        // if state of the current branch is 'behind', then pull
        if (strpos($gitStatus[0], 'behind') !== false) {
            exec('git pull 2>&1', $pullResponse);
        }

        return json_encode([
            'status'   => [
                'remote_update'=> $gitUpdate,
                'branch' => $gitStatus,
                'pull' => $pullResponse
            ]
        ]);

    }


    /**
     * @param string the IP where the request came from: $_SERVER['REMOTE_ADDR']
     * @param array the URI parameters: $_GET
     * @return bool true if the request is valid
     */
    private static function validRequest($remoteAddress, $parameters){

        // validate API key
        if(isset($parameters['api_key']) && self::$apiKey != $parameters['api_key']){
            return false;
        }

        // validate request is from whitelisted IP
        if(!self::isValidIp($remoteAddress, self::$bitbucketIps)){
            return false;
        }

        return true;
    }

    /**
     * Check if the remote address IP is in the listed
     * of whitelisted IPs
     * @param string $ip the IP to check against
     * @param array $whitelistedIps the allowed/whitelisted IPs
     * @return boolean true if a valid IP, otherwise false
     */
    public static function isValidIp($ip, $whitelistedIps){
        foreach ($whitelistedIps as $allowedIp){
            if(self::ip_in_range($ip, $allowedIp)){
                return true;
            }
        }
        return false;
    }

    /**
     * Check if a given ip is in a network
     * @param  string $ip    IP to check in IPV4 format eg. 127.0.0.1
     * @param  string $range IP/CIDR netmask eg. 127.0.0.0/24, also 127.0.0.1 is accepted and /32 assumed
     * @return boolean true if the ip is in this range / false if not.
     */
    public static function ip_in_range( $ip, $range ) {
        if ( strpos( $range, '/' ) == false ) {
            $range .= '/32';
        }
        // $range is in IP/CIDR format eg 127.0.0.1/24
        list( $range, $netmask ) = explode( '/', $range, 2 );
        $range_decimal = ip2long( $range );
        $ip_decimal = ip2long( $ip );
        $wildcard_decimal = pow( 2, ( 32 - $netmask ) ) - 1;
        $netmask_decimal = ~ $wildcard_decimal;
        return ( ( $ip_decimal & $netmask_decimal ) == ( $range_decimal & $netmask_decimal ) );
    }

}
