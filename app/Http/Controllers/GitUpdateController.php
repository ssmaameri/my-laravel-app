<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Services\GitUpdateService as GitUpdateService;


class GitUpdateController extends Controller
{

  public function index(){

      return GitUpdateService::update($_SERVER['REMOTE_ADDR'], $_GET);

  }

}
